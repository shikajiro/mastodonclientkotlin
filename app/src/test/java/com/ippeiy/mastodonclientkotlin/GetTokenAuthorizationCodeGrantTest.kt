package com.ippeiy.mastodonclientkotlin

import com.ippeiy.mastodonclientkotlin.domain.usecase.GetTokenAuthorizationCodeGrant
import com.ippeiy.mastodonclientkotlin.event.EventBus
import com.ippeiy.mastodonclientkotlin.event.RxBus
import com.ippeiy.mastodonclientkotlin.event.toview.RedirectToInstanceEvent
import com.ippeiy.mastodonclientkotlin.model.AuthModel
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.Scheduler
import org.junit.Test
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.util.concurrent.Executor


/**
 * Created by ippeiy on 2017/06/05.
 */
@RunWith(PowerMockRunner::class)
@PrepareForTest(RxBus::class)
class GetTokenAuthorizationCodeGrantTest {

    lateinit var authModel: AuthModel

    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }


    @Test
    fun testRedirect() {
        PowerMockito.mockStatic(RxBus::class.java)
        val clientId = "client_id"
        authModel = mock(AuthModel::class.java)
        whenever(authModel.getClientId()).thenReturn(Observable.just(clientId))
        val mObservable = mock(Observable::class.java) as Observable<Any>
        whenever(RxBus.observable).thenReturn(mObservable)
        whenever(RxBus.observable.subscribe()).then {}
        val getTokenUsecase = GetTokenAuthorizationCodeGrant(authModel)
        getTokenUsecase.execute()
        PowerMockito.verifyStatic()
        RxBus.send(com.nhaarman.mockito_kotlin.any<RedirectToInstanceEvent>())


    }

}