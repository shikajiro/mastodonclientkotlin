package com.ippeiy.mastodonclientkotlin

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.ippeiy.mastodonclient.entity.AccessToken
import com.ippeiy.mastodonclientkotlin.model.AuthModel
import com.ippeiy.mastodonclientkotlin.model.RepositoryAuth
import com.ippeiy.mastodonclientkotlin.model.WebApiAuthWrapper
import com.ippeiy.mastodonclientkotlin.model.db.Instance
import com.ippeiy.mastodonclientkotlin.model.entity.Client
import com.raizlabs.android.dbflow.kotlinextensions.from
import com.raizlabs.android.dbflow.kotlinextensions.select
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever


/**
 * Created by ippeiy on 2017/06/01.
 */
@RunWith(AndroidJUnit4::class)
class AuthModelTest {
    lateinit var authModel: AuthModel
    lateinit var webApiAuth: WebApiAuthWrapper
    lateinit var repo: RepositoryAuth
    val redirectUrl = "url"
    val name = "name"
    val scopes = "scopes"
    val instanceUrl = "url"

    @Before
    fun setUp() {
        webApiAuth = mock(WebApiAuthWrapper::class.java)
        repo = mock(RepositoryAuth::class.java)
        authModel = spy(AuthModel(InstrumentationRegistry.getContext(), repo, webApiAuth))
        doReturn(name).whenever(authModel).clientName()
        doReturn(redirectUrl).whenever(authModel).redirectUrl()
        doReturn(scopes).whenever(authModel).scopes()
    }

    @Test
    fun testGetClientWhenClientNotExist() {
        val observer = TestObserver<String>()
        whenever(repo.getInstance(instanceUrl)).thenReturn(emptyList())
        val clientId = "1"
        val client = Client(1, clientId, "")
        whenever(webApiAuth.registerApp(name, redirectUrl, scopes))
                .thenReturn(Observable.just(client))
        authModel.getClientId().subscribe(observer)
        verify(webApiAuth).registerApp(name, redirectUrl, scopes)
        observer.assertValue(clientId)
        assertThat((select from Instance::class).queryList(), hasSize(1))
    }

    @Test
    fun testGetClientWhenClientExist() {
        val observer = TestObserver<String>()
        val clientId = "2"
        val instance = Instance()
        instance.client_id = clientId
        val instanceUrl = "url"
        whenever(webApiAuth.url).thenReturn(instanceUrl)
        whenever(repo.getInstance(instanceUrl))
                .thenReturn(listOf<Instance>(instance))
        authModel.getClientId().subscribe(observer)
        verify(webApiAuth, never()).registerApp(name, redirectUrl, scopes)
        observer.assertValue(clientId)
    }

    @Test
    fun testTokenSaved() {
        val observer = TestObserver<String>()
        val clientId = "clientId1"
        val token = "token1"
        val tokenEntity = AccessToken(token, "", "", "")
        val instance = listOf<Instance>(Instance("", "", clientId, "", true))
        whenever(repo.getActiveInstances()).thenReturn(instance)
        whenever(webApiAuth.getTokenAuthCode(any(), any(), any(), any())).thenReturn(Observable.just(tokenEntity))
        val code = "code"
        authModel
                .requestToken(code)
                .subscribe(observer)
        observer.await()
        verify(repo).saveAccount(any(), com.nhaarman.mockito_kotlin.eq(token), any())

    }

}