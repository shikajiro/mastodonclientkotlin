package com.ippeiy.mastodonclientkotlin


import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.ippeiy.mastodonclientkotlin.model.RepositoryAuthImpl
import com.ippeiy.mastodonclientkotlin.model.db.Account
import com.ippeiy.mastodonclientkotlin.model.db.AuthDatabase
import com.ippeiy.mastodonclientkotlin.model.db.Instance
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.delete
import com.raizlabs.android.dbflow.kotlinextensions.save
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by ippeiy on 2017/05/30.
 */
@RunWith(AndroidJUnit4::class)
class RepositoryAuthTest {
    val account1: Account = Account()
    val account2: Account = Account()
    val instance: Instance = Instance()
    val repository = RepositoryAuthImpl(InstrumentationRegistry.getTargetContext())
    val url = "url"

    @Before
    fun setUp() {
        account1.id = "1"
        account1.instanceId = "1"
        account1.isActive = true
        account1.name = "user1"
        account1.token = "xxxxx"
        account2.id = "2"
        account2.instanceId = "1"
        account2.isActive = false
        account2.name = "user1"
        account2.token = "xxxxx"

        instance.id = "1"
        instance.client_id = "client_id"
        instance.client_secret = "secret"
        instance.client_name = "name"
        instance.url = url
        instance.isActive = true
    }

    @Test
    fun testGetAccount() {
        account1.save()
        account2.save()
        val fetchedAccount = repository.getActiveAccounts()
        assertThat(fetchedAccount, hasSize(1))
        assertThat(fetchedAccount.first().id, `is`("1"))
    }

    @Test
    fun testGetInstance() {
        instance.save()
        val fetchedInstance = repository.getInstance(url)
        assertThat(fetchedInstance, hasSize(1))
        assertThat(fetchedInstance.first().url, `is`(url))
        instance.delete()
    }

    @Test
    fun testGetActiveInstance() {
        account1.save()
        instance.save()
        val fetchedInstance = repository.getActiveInstances()
        assertThat(fetchedInstance, hasSize(1))
        assertThat(fetchedInstance.first().url, `is`(url))
        instance.delete()
    }

    @Test
    fun testDeactivateAccounts() {
        account1.save()
        account2.save()
        repository.deactivateAccounts()
        val fetchedAccountAll = repository.getAllAccounts()
        assertThat(fetchedAccountAll, hasSize(2))
        val fetchedAccount = repository.getActiveAccounts()
        assertThat(fetchedAccount, hasSize(0))
    }

    @Test
    fun testDeactivateInstances() {
        instance.save()
        repository.deactivateInstances()
        val fetchedInstanceAll = repository.getAllInstances()
        assertThat(fetchedInstanceAll, hasSize(1))
        val fetchedInstance = repository.getActiveInstances()
        assertThat(fetchedInstance, hasSize(0))
    }

    @Test
    fun testSaveAccount() {
        account1.save()
        val token = "token2"
        repository.saveAccount("1", token, true)
        val fetchedAccount = repository.getActiveAccounts()
        assertThat(fetchedAccount, hasSize(1))
        assertThat(fetchedAccount.first().token, `is`(token))
        assertThat(repository.getAccount(account1.id!!)!!.isActive, `is`(false))

    }

    @Test
    fun testSaveInstance() {
        instance.save()
        val url = "url2"
        repository.saveInstance(url, "", "", "", true)
        val fetchedInstance = repository.getActiveInstances()
        assertThat(fetchedInstance, hasSize(1))
        assertThat(fetchedInstance.first().url, `is`(url))
        assertThat(repository.getInstance(instance.url!!).first().isActive, `is`(false))
    }

    @Test
    fun testPrimaryKey() {
        val account = Account()
        account.instanceId = "1"
        account.isActive = true
        account.name = "user1"
        account.token = "xxxxx"
        account.save()
        val account2 = Account()
        account.instanceId = "1"
        account.isActive = true
        account.name = "user1"
        account.token = "xxxxx"
        account2.save()
        val accounts = repository.getAllAccounts()
        assertThat(accounts.first().id, not(accounts.get(1).id))
    }

    private inline fun <reified T> dropTable() {
        val modelAdapter = FlowManager.getModelAdapter<T>(T::class.java)
        val database = FlowManager.getWritableDatabase(AuthDatabase::class.java)
        database.execSQL("DROP TABLE IF EXISTS " + modelAdapter.getTableName())
        database.execSQL(modelAdapter.getCreationQuery())
    }

    @After
    fun tearDown() {
        dropTable<Account>()
        dropTable<Instance>()
    }
}