package com.ippeiy.mastodonclientkotlin

import android.app.Instrumentation
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.ippeiy.mastodonclientkotlin.auth.InitialActivity
import com.ippeiy.mastodonclientkotlin.main.MainActivity
import com.ippeiy.mastodonclientkotlin.model.db.Account
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.delete
import com.raizlabs.android.dbflow.kotlinextensions.save
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.rule.ActivityTestRule
import com.ippeiy.mastodonclientkotlin.auth.InstanceLoginActivity
import org.junit.Rule


/**
 * Created by ippeiy on 2017/05/30.
 */
@RunWith(AndroidJUnit4::class)
class InitialActivityTest {
    val mMainActivityMonitor = Instrumentation.ActivityMonitor(MainActivity::class.java.name, null, false);
    val mInstanceLoginActivityMonitor = Instrumentation.ActivityMonitor(InstanceLoginActivity::class.java.name, null, false);
    val account1: Account = Account()
    @Rule
    @JvmField
    var activityRule = ActivityTestRule<InitialActivity>(InitialActivity::class.java, true, false)
    val intent: Intent = Intent();


    @Before
    fun setUp() {
        FlowManager.init(InstrumentationRegistry.getTargetContext())
        InstrumentationRegistry.getInstrumentation().addMonitor(mMainActivityMonitor)
        InstrumentationRegistry.getInstrumentation().addMonitor(mInstanceLoginActivityMonitor)
        account1.id = "1"
        account1.instanceId = "1"
        account1.isActive = true
        account1.name = "user1"
        account1.token = "xxxxx"
    }

    @Test
    fun testInstanceLoginActivityStart() {
        account1.delete()
        activityRule.launchActivity(intent);
        val activity = mInstanceLoginActivityMonitor.waitForActivityWithTimeout(3 * 1000)
        Assert.assertNotNull("Activity was not started", activity)
    }

    @Test
    fun testMainActivityStart() {
        account1.save()
        activityRule.launchActivity(intent);
        val activity = mMainActivityMonitor.waitForActivityWithTimeout(3 * 1000)
        Assert.assertNotNull("Activity was not started", activity)
    }

    @After
    fun tearDown() {
        account1.delete()
    }
}
