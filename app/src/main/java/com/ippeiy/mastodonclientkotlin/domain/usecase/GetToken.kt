package com.ippeiy.mastodonclientkotlin.domain.usecase

/**
 * Created by ippeiy on 2017/06/05.
 */
interface GetToken {
    fun execute()
}
