package com.ippeiy.mastodonclientkotlin.domain.usecase

import android.util.Log
import com.ippeiy.mastodonclientkotlin.event.EventBus
import com.ippeiy.mastodonclientkotlin.event.RxBus
import com.ippeiy.mastodonclientkotlin.event.fromview.RequestTokenEvent
import com.ippeiy.mastodonclientkotlin.event.toview.ErrorGetClientIdEvent
import com.ippeiy.mastodonclientkotlin.event.toview.ErrorRequestTokenEvent
import com.ippeiy.mastodonclientkotlin.event.toview.RedirectToInstanceEvent
import com.ippeiy.mastodonclientkotlin.event.toview.StartMainActivityEvent
import com.ippeiy.mastodonclientkotlin.ext.observeOnMainThread
import com.ippeiy.mastodonclientkotlin.ext.subscribeOnNewThread
import com.ippeiy.mastodonclientkotlin.model.AuthModel

/**
 * Created by ippeiy on 2017/06/05.
 */
class GetTokenAuthorizationCodeGrant(val authModel: AuthModel) : GetToken {
    override fun execute() {
        authModel.getClientId()
                .subscribeOnNewThread()
                .observeOnMainThread()
                .subscribe(
                        {
                            clientId ->
                            RxBus.send(RedirectToInstanceEvent(clientId))
                            RxBus.observable
                                    .subscribeOnNewThread()
                                    .subscribe({ event ->
                                        when (event) {
                                            is RequestTokenEvent -> {
                                                authModel.requestToken(event.code)
                                                        .subscribeOnNewThread()
                                                        .observeOnMainThread()
                                                        .subscribe(
                                                                {
                                                                    token ->
                                                                    RxBus.send(StartMainActivityEvent(token))
                                                                }
                                                                ,
                                                                {
                                                                    error ->
                                                                    RxBus.send(ErrorRequestTokenEvent(error))
                                                                    Log.d("requestToken", error.stackTrace.joinToString(separator = "\n"))
                                                                })
                                            }
                                        }
                                    })
                        },
                        {
                            error ->
                            RxBus.send(ErrorGetClientIdEvent(error))
                            Log.d("clientId", error.stackTrace.joinToString(separator = "\n"))
                        })
    }

}