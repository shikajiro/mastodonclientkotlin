package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Relationship(val id: Int,
                        val following: Boolean,
                        val followedBy:  Boolean,
                        val blocking: Boolean,
                        val muting: Boolean,
                        val requested: Boolean)
