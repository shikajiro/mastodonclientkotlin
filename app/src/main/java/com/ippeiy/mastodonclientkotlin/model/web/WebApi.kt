package com.ippeiy.mastodonclient

import com.ippeiy.mastodonclient.entity.AccessToken
import com.ippeiy.mastodonclient.entity.Account
import com.ippeiy.mastodonclient.entity.Attachment
import com.ippeiy.mastodonclient.entity.Card
import com.ippeiy.mastodonclient.entity.Context
import com.ippeiy.mastodonclient.entity.Instance
import com.ippeiy.mastodonclient.entity.Notification
import com.ippeiy.mastodonclient.entity.Relationship
import com.ippeiy.mastodonclient.entity.Report
import com.ippeiy.mastodonclient.entity.Results
import com.ippeiy.mastodonclient.entity.Status
import io.reactivex.Observable

import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by ippeiy on 2017/04/19.
 */

interface WebApi {
    @GET("/api/v1/accounts/{id}")
    fun getAccount(@Path("id") id: Int): Observable<Account>

    @GET("/api/v1/accounts/verify_credentials")
    fun verifyCredentials(): Observable<Account>

    @PATCH("/api/v1/accounts/update_credentials")
    fun updateCredentials(@QueryMap queryMap: Map<String, String>): Observable<Void>

    @GET("/api/v1/accounts/{id}/followers")
    fun getFollowers(@Path("id") id: String): Observable<List<Account>>

    @GET("/api/v1/accounts/{id}/following")
    fun getFollowing(@Path("id") id: String): Observable<List<Account>>


    @GET("/api/v1/accounts/{id}/statuses")
    fun getStatuses(@Path("id") id: Int, @Query("only_media") onlyMedia: String, @Query("exclude_replies") excludeReplies: String, @Query("since_id") sinceId: Int?, @Query("limit") limit: Int?): Observable<List<Status>>

    @POST("/api/v1/accounts/{id}/follow")
    fun follow(@Path("id") id: String): Observable<Relationship>

    @POST("/api/v1/accounts/{id}/unfollow")
    fun unfollow(@Path("id") id: String): Observable<Relationship>

    @POST("/api/v1/accounts/{id}/block")
    fun block(@Path("id") id: String): Observable<Relationship>

    @POST("/api/v1/accounts/{id}/unblock")
    fun unblock(@Path("id") id: String): Observable<Relationship>

    @POST("/api/v1/accounts/{id}/mute")
    fun mute(@Path("id") id: String): Observable<Relationship>

    @POST("/api/v1/accounts/{id}/unmute")
    fun unmute(@Path("id") id: String): Observable<Relationship>

    //:Todo http://stackoverflow.com/questions/37254232/how-to-post-array-in-retrofit-android
    @GET("/api/v1/accounts/relationships")
    fun getRelationship(@Query("id") id: String): Observable<List<Relationship>>

    @GET("/api/v1/accounts/search")
    fun searchAccounts(@Query("q") q: String, @Query("limit") limit: Int): Observable<List<Account>>

    @GET("/api/v1/blocks")
    fun blockedUsers(): Observable<List<Account>>

    @GET("/api/v1/favourites")
    fun favourites(): Observable<List<Status>>

    @GET("/api/v1/follow_requests")
    fun followRequests(): Observable<List<Account>>

    @POST("/api/v1/follow_requests/{id}/authorize")
    fun authorize(@Path("id") id: String): Observable<Void>

    @POST("/api/v1/follow_requests/{id}/reject")
    fun reject(@Path("id") id: String): Observable<Void>

    @POST("/api/v1/follows")
    fun follows(@Query("uri") uri: String): Observable<Account>

    @GET("/api/v1/instance")
    fun insatnce(): Observable<Instance>

    @POST("/api/v1/media")
    fun uploadMedia(@Query("file") file: String): Observable<Attachment>

    @GET("/api/v1/mutes")
    fun mutes(): Observable<List<Account>>

    @GET("/api/v1/notifications")
    fun notifications(): Observable<List<Notification>>

    @GET("/api/v1/notifications/{id}")
    fun getNotification(@Path("id") id: String): Observable<Notification>

    @POST("/api/v1/notifications/clear")
    fun clearNotifications(): Observable<Notification>

    @GET("/api/v1/reports")
    fun reports(): Observable<List<Report>>

    @POST("/api/v1/reports")
    fun reports(@Query("account_id") id: String, @Query("status_ids") statuses: String, @Query("comment") comment: String): Observable<Report>

    @GET("/api/v1/search")
    fun search(@Query("q") q: String, @Query("resolve") resolve: String): Observable<Results>

    @GET("/api/v1/statuses/{id}")
    fun getStatus(@Path("id") id: String): Observable<Status>

    @GET("/api/v1/statuses/{id}/context")
    fun getContext(@Path("id") id: String): Observable<Context>

    @GET("/api/v1/statuses/{id}/card")
    fun getCard(@Path("id") id: String): Observable<Card>

    @GET("/api/v1/statuses/{id}/reblogged_by")
    fun rebloggedBy(@Path("id") id: String): Observable<List<Account>>

    @GET("/api/v1/statuses/{id}/favourited_by")
    fun favouritedBy(@Path("id") id: String): Observable<List<Account>>

    @POST("/api/v1/statuses")
    fun postStatus(@Query("status") status: String,
                   @Query("in_reply_to_id") inReplyToId: Int?,
                   @Query("media_ids") mediaIds: List<Int>,
                   @Query("sensitive") sensitive: Boolean?,
                   @Query("spoiler_text") spolilerText: String,
                   @Query("visibility") visibility: String): Observable<Status>

    @DELETE("/api/v1/statuses/{id}")
    fun deleteStatus(@Path("id") id: Int): Observable<Void>

    @POST("/api/v1/statuses/{id}/reblog")
    fun reblog(@Path("id") id: Int): Observable<Status>

    @POST("/api/v1/statuses/{id}/unreblog")
    fun unreblog(@Path("id") id: Int): Observable<Status>

    @POST("/api/v1/statuses/{id}/favourite")
    fun favourite(@Path("id") id: Int): Observable<Status>

    @POST("/api/v1/statuses/{id}/unfavourite")
    fun unfavourite(@Path("id") id: Int): Observable<Status>

    @GET("/api/v1/timelines/home")
    fun homeTimeline(): Observable<List<Status>>

    @GET("/api/v1/timelines/public")
    fun getPublicTimeline(@Query("local") local: String): Observable<List<Status>>

    @GET("/api/v1/timelines/tag/{:hashtag}")
    fun getPublicTimeline(@Path("hashtag") hashtag: String, @Query("local") local: String): Observable<List<Status>>

}
