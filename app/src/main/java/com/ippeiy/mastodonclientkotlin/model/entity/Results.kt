package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Results(val accounts: List<Account>?,
                   val statuses: List<Status>?,
                   val hashtags: List<String>?)
