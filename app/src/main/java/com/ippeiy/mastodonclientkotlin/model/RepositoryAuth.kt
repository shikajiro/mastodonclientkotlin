package com.ippeiy.mastodonclientkotlin.model

import android.content.Context
import com.ippeiy.mastodonclientkotlin.model.db.*
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.from
import com.raizlabs.android.dbflow.kotlinextensions.save
import com.raizlabs.android.dbflow.kotlinextensions.select
import com.raizlabs.android.dbflow.kotlinextensions.where

interface RepositoryAuth {
    fun getActiveAccounts(): List<Account>
    fun getInstance(url: String): List<Instance>
    fun getActiveInstances(): List<Instance>
    fun saveInstance(url: String, clientName: String, clientId: String, clientSecret: String, isActive: Boolean)
    fun saveAccount(instanceId: String, token: String, isActive: Boolean)
}

/**
 * Created by ippeiy on 2017/06/01.
 */
class RepositoryAuthImpl(context: Context) : RepositoryAuth {
    init {
        FlowManager.init(context)
    }


    internal fun getAccount(id: String): Account? {
        return (select from Account::class where (Account_Table.id.eq(id))).querySingle()
    }

    internal fun getAllAccounts(): List<Account> {
        return (select from Account::class).queryList()
    }

    internal fun getAllInstances(): List<Instance> {
        return (select from Instance::class).queryList()
    }


    override fun getActiveAccounts(): List<Account> {
        return (select from Account::class where (Account_Table.isActive.eq(true)))
                .queryList()
    }

    override fun getInstance(url: String): List<Instance> {
        return (select from Instance::class where (Instance_Table.url.eq(url))).queryList()
    }

    override fun getActiveInstances(): List<Instance> {
        return (select from Instance::class where (Instance_Table.isActive.eq(true)))
                .queryList()
    }

    internal fun deactivateAccounts() {
        val allAccounts = (select from Account::class).queryList()
        for (account in allAccounts) {
            account.isActive = false
            account.save()
        }
    }


    internal fun deactivateInstances() {
        val allInstances = (select from Instance::class).queryList()
        for (instance in allInstances) {
            instance.isActive = false
            instance.save()

        }
    }

    override fun saveInstance(url: String,
                              clientName: String,
                              clientId: String,
                              clientSecret: String,
                              isActive: Boolean) {
        FlowManager.getDatabase(AuthDatabase::class.java)
                .beginTransactionAsync({
                    if (isActive) {
                        deactivateInstances()
                    }
                    Instance(url, clientName, clientId, clientSecret, isActive).save()
                })
                .build()
                .executeSync()

    }

    override fun saveAccount(instanceId: String,
                             token: String,
                             isActive: Boolean) {
        FlowManager.getDatabase(AuthDatabase::class.java)
                .beginTransactionAsync({
                    if (isActive) {
                        deactivateAccounts()
                    }
                    Account(instanceId, token, isActive).save()
                })
                .build()
                .executeSync()

    }

}