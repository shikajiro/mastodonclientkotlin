package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Account(val id: Int,
                   val username: String,
                   val acct: String,
                   val displayName: String,
                   val locked: Boolean,
                   val createdAt: String,
                   val followersCount: String,
                   val followingCount: String,
                   val statusesCount: String,
                   val note: String,
                   val url: String,
                   val avatar: String,
                   val avatarStatic: String,
                   val header: String,
                   val headerStatic: String)
