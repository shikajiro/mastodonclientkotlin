package com.ippeiy.mastodonclientkotlin.model.db

import java.util.*

/**
 * Created by ippeiy on 2017/05/26.
 */
@com.raizlabs.android.dbflow.annotation.Table(database = AuthDatabase::class)
class Account() {
    constructor(instanceId: String, token: String, isActive: Boolean): this() {
        this.instanceId = instanceId
        this.token = token
        this.isActive = isActive
    }
    @com.raizlabs.android.dbflow.annotation.PrimaryKey var id: String? = UUID.randomUUID().toString()
    @com.raizlabs.android.dbflow.annotation.Column var instanceId: String? = null
    @com.raizlabs.android.dbflow.annotation.Column var name: String? = null
    @com.raizlabs.android.dbflow.annotation.Column var token: String? = null
    @com.raizlabs.android.dbflow.annotation.Column var isActive: Boolean? = null
}
