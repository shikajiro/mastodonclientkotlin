package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Mention(val url: String,
                   val username: String,
                   val acct: String,
                   val id: Int)
