package com.ippeiy.mastodonclientkotlin.model

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.ippeiy.mastodonclient.WebApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by ippeiy on 2017/05/26.
 */
object Model {
    private var mUrl: String? = null
    lateinit var webApi: WebApi
    val GSON = GsonBuilder().setFieldNamingPolicy(
            FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    fun initializeWebApi(url: String) {
        if (mUrl != url) {
            val retrofit = Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(GSON))
                    .build()
            webApi = retrofit.create<WebApi>(WebApi::class.java)
        }
    }
}