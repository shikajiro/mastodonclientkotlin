package com.ippeiy.mastodonclientkotlin.model

import android.content.Context
import android.content.res.Resources
import com.ippeiy.mastodonclientkotlin.R
import io.reactivex.Observable

/**
 * Created by ippeiy on 2017/05/29.
 */

open class AuthModel constructor(val context: Context, val repoAuth: RepositoryAuth, val webApiWrapper: WebApiAuthWrapper) {
    fun resources(): Resources = context.resources
    fun redirectUrlHost() = resources().getString(R.string.redirect_uri_host)
    fun redirectUrlschema() = resources().getString(R.string.redirect_uri_scheme)
    open fun redirectUrl() = redirectUrlschema() + "://" + redirectUrlHost()
    open fun clientName() = resources().getString(R.string.client_name)
    open fun scopes() = resources().getString(R.string.scopes)
    val authorizePath = "/oauth/authorize"
    fun buildBrowserRedirectUrl(instanceUrl: String, clientId: String) =
            "%s/%s/?client_id=%s&redirect_uri=%s&response_type=code&scope=%s"
                    .format(instanceUrl, authorizePath, clientId, redirectUrl(), scopes()).replace(" ", "%20")

    open fun getClientId(): Observable<String> =
            Observable.just(repoAuth.getInstance(webApiWrapper.url))
                    .flatMap { instanceList ->
                        if (instanceList!!.isEmpty()) {
                            webApiWrapper.registerApp(clientName(), redirectUrl(), scopes())
                                    .doOnNext { client ->
                                        repoAuth.saveInstance(webApiWrapper.url, clientName(), client.clientId, client.clientSecret, true)
                                    }
                                    .map { client ->
                                        client.clientId
                                    }
                        } else {
                            Observable.just(instanceList.first().client_id)
                        }
                    }

    fun requestToken(code: String): Observable<String> {
        val instance = repoAuth.getActiveInstances().first()
        return webApiWrapper
                .getTokenAuthCode(instance.client_id!!, instance.client_secret!!, code, redirectUrl())
                .doOnNext { repoAuth.saveAccount(instance.id, it.accessToken, true) }
                .map { it.accessToken }
    }

    companion object {
        val TOKEN = "token"
    }
}

