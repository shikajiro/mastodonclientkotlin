package com.ippeiy.mastodonclientkotlin.model.db

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by ippeiy on 2017/05/26.
 */
@Database(name = AuthDatabase.NAME, version = AuthDatabase.VERSION)
object AuthDatabase {
    const val NAME = "AuthDatabase"
    const val VERSION = 1
}