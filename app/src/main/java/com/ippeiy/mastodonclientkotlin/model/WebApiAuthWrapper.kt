package com.ippeiy.mastodonclientkotlin.model

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.ippeiy.mastodonclient.entity.AccessToken
import com.ippeiy.mastodonclientkotlin.model.entity.Client
import com.ippeiy.mastodonclientkotlin.model.web.WebApiAuth
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by ippeiy on 2017/06/05.
 */
open class WebApiAuthWrapper(open val url: String) : WebApiAuth {
    val GSON = GsonBuilder().setFieldNamingPolicy(
            FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()
    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(GSON))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    val webApi = retrofit.create<WebApiAuth>(WebApiAuth::class.java)

    override fun registerApp(clientName: String, redirectUri: String, scopes: String): Observable<Client> {
        return webApi.registerApp(clientName, redirectUri, scopes)
    }

    override fun getToken(grantType: String, clientId: String, clientSecret: String, username: String, pasword: String): Observable<AccessToken> {
        return webApi.getToken(grantType, clientId, clientSecret, username, pasword)
    }

    override fun getTokenAuthCode(clientId: String, clientSecret: String, code: String, redirectUri: String): Observable<AccessToken> {
        return webApi.getTokenAuthCode(clientId, clientSecret, code, redirectUri)
    }

}