package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Tag(val name: String, val url: String)
