package com.ippeiy.mastodonclientkotlin.model.db

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import java.util.*

/**
 * Created by ippeiy on 2017/05/26.
 */
@Table(database = AuthDatabase::class)
class Instance() {
    constructor(url: String, clientName: String, clientId: String, clientSecret: String, isActive: Boolean): this() {
        this.url = url
        this.client_name = clientName
        this.client_id = clientId
        this.client_secret = clientSecret
        this.isActive = isActive
    }
    @PrimaryKey var id: String = UUID.randomUUID().toString()
    @Column var url: String? = null
    @Column var client_name: String? = null
    @Column var client_id: String? = null
    @Column var client_secret: String? = null
    @Column var isActive: Boolean = false
}
