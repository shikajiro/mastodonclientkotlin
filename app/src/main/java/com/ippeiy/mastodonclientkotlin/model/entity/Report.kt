package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Report(val id: Int,
                  val actionTaken: String)
