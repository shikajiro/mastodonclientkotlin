package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Notification(val id: String,
                        val type: String,
                        val createdAt: String,
                        val account: Account,
                        val status: List<Status>?)
