package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on
 */


data class Status(val id: Int,
                  val uri: String,
                  val url: String,
                  val account: Account,
                  val inReplyToId: Int?,
                  val inReplyToAccountId: Int?,
                  val reblog: Status?,
                  val content: String,
                  val createdAt: String,
                  val reblogsCount: Int,
                  val favouritesCount: Int,
                  val reblogged: Boolean?,
                  val favourited: Boolean?,
                  val sensitive: Boolean?,
                  val spoilerText: String,
                  val visibility: String,
                  val mediaAttachments: List<Attachment>,
                  val mentions: List<Mention>,
                  val tags: List<Tag>,
                  val application: Application?,
                  val language: String)
