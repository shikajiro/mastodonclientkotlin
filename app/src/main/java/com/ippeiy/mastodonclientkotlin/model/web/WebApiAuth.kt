package com.ippeiy.mastodonclientkotlin.model.web

import com.ippeiy.mastodonclient.entity.AccessToken
import com.ippeiy.mastodonclientkotlin.model.entity.Client
import io.reactivex.Observable
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by ippeiy on 2017/05/31.
 */
interface WebApiAuth {
    @POST("/api/v1/apps")
    fun registerApp(@Query("client_name") clientName: String,
                    @Query("redirect_uris") redirectUri: String,
                    @Query("scopes") scopes: String): Observable<Client>

    @POST("/oauth/token")
    fun getToken(@Query("grant_type") grantType: String, @Query("client_id") clientId: String,
                 @Query("client_secret") clientSecret: String, @Query("username") username: String,
                 @Query("password") pasword: String): Observable<AccessToken>

    @POST("/oauth/token?grant_type=authorization_code")
    fun getTokenAuthCode(@Query("client_id") clientId: String,
                         @Query("client_secret") clientSecret: String, @Query("code") code: String,
                         @Query("redirect_uri") redirectUri: String): Observable<AccessToken>
}