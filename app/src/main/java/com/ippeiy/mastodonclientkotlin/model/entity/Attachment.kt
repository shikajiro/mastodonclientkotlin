package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Attachment(val id: String,
                      val type: String,
                      val url: String,
                      val remoteUrl: String?,
                      val previewUrl: String,
                      val textUrl: String?)

