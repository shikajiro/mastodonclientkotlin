package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/20.
 */

data class AccessToken(
        val accessToken: String,
        val tokenType: String,
        val scope: String,
        val createdAt: String)
