package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Application(val name: String,
                       val website: String)
