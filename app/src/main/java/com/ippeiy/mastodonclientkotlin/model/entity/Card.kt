package com.ippeiy.mastodonclient.entity

/**
 * Created by ippeiy on 2017/04/19.
 */

data class Card(val url: String,
                val title: String,
                val description: String,
                val image: String?)
