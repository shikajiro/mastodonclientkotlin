package com.ippeiy.mastodonclientkotlin.model.db

/**
 * Created by ippeiy on 2017/05/26.
 */
@com.raizlabs.android.dbflow.annotation.Table(database = AuthDatabase::class)
class AccountTable() {
    @com.raizlabs.android.dbflow.annotation.PrimaryKey var id: Int = 0
    @com.raizlabs.android.dbflow.annotation.Column var instanceId: Int? = 0
    @com.raizlabs.android.dbflow.annotation.Column var name: String? = null
    @com.raizlabs.android.dbflow.annotation.Column var token: String? = null
    @com.raizlabs.android.dbflow.annotation.Column var isActive: Boolean? = null
}
