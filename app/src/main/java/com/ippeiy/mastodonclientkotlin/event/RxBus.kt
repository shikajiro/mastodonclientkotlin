package com.ippeiy.mastodonclientkotlin.event

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

/**
 * Created by ippeiy on 2017/05/31.
 */

object RxBus {
    private val bus = PublishSubject.create<Any>()

    @JvmStatic
    fun send(event: Any) {
        bus.onNext(event)
    }

    @JvmStatic
    val observable: Observable<Any>
        get() = bus

    val observableOnMainThread: Observable<Any>
        get() = observable.observeOn(AndroidSchedulers.mainThread())
}