package com.ippeiy.mastodonclientkotlin.event.fromview

import android.net.Uri

/**
 * Created by ippeiy on 2017/06/05.
 */
class RequestTokenEvent(uri: Uri) {
    val code = uri.toString().substring(uri.toString().indexOf("=") + 1)
}