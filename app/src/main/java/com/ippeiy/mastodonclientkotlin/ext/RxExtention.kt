package com.ippeiy.mastodonclientkotlin.ext

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by ippeiy on 2017/05/31.
 */
fun <T> Observable<T>.observeOnMainThread() = observeOn(AndroidSchedulers.mainThread())
fun <T> Observable<T>.subscribeOnNewThread() = subscribeOn(Schedulers.newThread())
fun <T> Observable<T>.observeOnNewThread() = observeOn(Schedulers.newThread())
