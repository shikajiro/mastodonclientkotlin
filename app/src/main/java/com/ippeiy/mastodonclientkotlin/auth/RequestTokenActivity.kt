package com.ippeiy.mastodonclientkotlin.auth

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ippeiy.mastodonclientkotlin.R
import com.ippeiy.mastodonclientkotlin.event.RxBus
import com.ippeiy.mastodonclientkotlin.event.fromview.RequestTokenEvent
import com.ippeiy.mastodonclientkotlin.event.toview.StartMainActivityEvent
import com.ippeiy.mastodonclientkotlin.main.MainActivity
import com.ippeiy.mastodonclientkotlin.model.AuthModel

class RequestTokenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_token)
        val intent = intent
        val uri = intent.data
        RxBus.send(RequestTokenEvent(uri))
        RxBus.observableOnMainThread.subscribe({ event ->
            when (event) {
                is StartMainActivityEvent -> startMainActivity(event.token)
            }
        })
    }

    fun startMainActivity(token: String) {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.putExtra(AuthModel.TOKEN, token)
        startActivity(intent)
    }
}
