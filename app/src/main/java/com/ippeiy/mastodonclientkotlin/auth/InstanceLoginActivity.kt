package com.ippeiy.mastodonclientkotlin.auth

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ippeiy.mastodonclientkotlin.R

class InstanceLoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instance_login)
        val tasksFragment: InstanceLoginFragment = (supportFragmentManager.findFragmentById(R.id.contentFrame) ?:
                InstanceLoginFragment()) as InstanceLoginFragment
        supportFragmentManager.beginTransaction()
                .add(R.id.contentFrame, tasksFragment)
                .commit()
    }
}

