package com.ippeiy.mastodonclientkotlin.auth

import android.content.Intent
import com.ippeiy.mastodonclientkotlin.MastodonApplication
import com.ippeiy.mastodonclientkotlin.main.MainActivity
import com.ippeiy.mastodonclientkotlin.model.AuthModel
import com.ippeiy.mastodonclientkotlin.model.RepositoryAuth

class InitialActivity : android.support.v7.app.AppCompatActivity() {
    lateinit var repositoryAuth: RepositoryAuth

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.ippeiy.mastodonclientkotlin.R.layout.activity_initial)
        repositoryAuth = (application as MastodonApplication).repoAuth
        val activeAccount = repositoryAuth.getActiveAccounts()
        if (activeAccount.isEmpty()) {
            startInstanceLoginActivity()
        } else {
            startMainActivity(activeAccount.first().token!!)
        }
    }

    fun startMainActivity(token: String) {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.putExtra(AuthModel.TOKEN, token)
        startActivity(intent)
    }

    fun startInstanceLoginActivity() {
        val intent = Intent(applicationContext, InstanceLoginActivity::class.java)
        startActivity(intent)
    }

}
