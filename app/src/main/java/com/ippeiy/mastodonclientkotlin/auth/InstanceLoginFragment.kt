package com.ippeiy.mastodonclientkotlin.auth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.ippeiy.mastodonclientkotlin.R
import com.ippeiy.mastodonclientkotlin.databinding.InstanceLoginFragmentBinding
import android.support.design.widget.Snackbar
import com.ippeiy.mastodonclientkotlin.MastodonApplication
import com.ippeiy.mastodonclientkotlin.domain.usecase.GetTokenAuthorizationCodeGrant
import com.ippeiy.mastodonclientkotlin.event.EventBus
import com.ippeiy.mastodonclientkotlin.event.RxBus
import com.ippeiy.mastodonclientkotlin.event.toview.ErrorGetClientIdEvent
import com.ippeiy.mastodonclientkotlin.event.toview.RedirectToInstanceEvent
import com.ippeiy.mastodonclientkotlin.model.*

/**
 * Created by ippeiy on 2017/05/30.
 */
class InstanceLoginFragment : Fragment() {
    private lateinit var loginButton: Button
    private lateinit var urlTextView: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var authModel: AuthModel

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater!!.inflate(R.layout.instance_login_fragment, container, false)
        val bd = InstanceLoginFragmentBinding.bind(root)
        loginButton = bd.insntaceLoginButton
        urlTextView = bd.instanceUrl
        progressBar = bd.loginProgress
        loginButton.setOnClickListener(instanceLoginListener)
        return root
    }

    fun getInstanceUrl() = urlTextView.text.toString()

    val instanceLoginListener = { _: View ->
        val url = getInstanceUrl()
        login(url)
    }

    fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    fun showErrorMessage(error: Throwable) {
        //Todo: ちゃんとしたエラーメッセージにする
        Snackbar.make(view!!, error.localizedMessage, Snackbar.LENGTH_LONG).show()
    }

    fun login(instanceUrl: String) {
        showProgressBar()
        val repoAuth = (context.applicationContext as MastodonApplication).repoAuth
        authModel = AuthModel(context, repoAuth, WebApiAuthWrapper(instanceUrl))
        GetTokenAuthorizationCodeGrant(authModel).execute()
        RxBus.observableOnMainThread.subscribe({ event ->
            hideProgressBar()
            when (event) {
                is RedirectToInstanceEvent ->
                    redirectToUrl(instanceUrl, event.clientId)
                is ErrorGetClientIdEvent ->
                    showErrorMessage(event.error)
            }
        })
    }

    fun redirectToUrl(instanceUrl: String, clientId: String) {
        val redirectUrl = authModel.buildBrowserRedirectUrl(instanceUrl, clientId)
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(redirectUrl)))
    }

}
