package com.ippeiy.mastodonclientkotlin

import android.app.Application
import com.ippeiy.mastodonclientkotlin.model.RepositoryAuth
import com.ippeiy.mastodonclientkotlin.model.RepositoryAuthImpl
import com.ippeiy.mastodonclientkotlin.model.web.WebApiAuth

/**
 * Created by ippeiy on 2017/06/01.
 */
class MastodonApplication : Application() {
    lateinit var repoAuth: RepositoryAuth
    lateinit var webApiAuth: WebApiAuth

    override fun onCreate() {
        super.onCreate()
        repoAuth = RepositoryAuthImpl(applicationContext)
    }
}